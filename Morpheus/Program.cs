﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

/**
 * Author : Terrence Sng
 * Declaration : I did not have prior knowledge of C#, solutions from this project has been referred from online sources like StackOverFlow and C# libray from Microsoft
 */

namespace Morpheus
{
    class Member
    {
        public int Id;
        public String Birthday{ get; set; }
        public String Address { get; set; }
        public String Sex { get; set; }
    }

    class Program
    {
        
        static DateTime RandomDay()
        {
            Random gen = new Random();
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(gen.Next(range));
        }
       
        static void Main(string[] args)
        {
            //Question 1
            String test = "Hello Woorlld";
            //removeDuplicates(test);
            //Question 2
            String fileText = ReadFileAsync("test.txt").GetAwaiter().GetResult();
            Console.WriteLine(fileText);
            //Question 3
            StartMultipleThread();
            //Question 4
            SplitFile("dummy.txt", 10000000);
            //Question 5
            createMultipleMemeber();
        }

        static void ThreadProc(Object stateInfo)
        {
            // No state object was passed to QueueUserWorkItem, so stateInfo is null.
            Console.WriteLine("Hello from the thread pool.");
        }

        static public async Task<String> ReadFileAsync(string documentName)
        {
            using (var reader = File.OpenText(documentName))
            {
                String fileText = "";
                try
                {
                    fileText = await reader.ReadToEndAsync();
                }
                catch (Exception E)
                {
                    Console.WriteLine(E.StackTrace);
                }
                return fileText;
            }
        }

        static void removeDuplicates(String input)
        {
            List<char> characters = new List<char>();
            char[] testCharacters = input.ToCharArray();
            foreach (char c in testCharacters)
            {
                if (characters.Contains(c) == false)
                {
                    characters.Add(c);
                }
            }
            Console.WriteLine(new String(characters.ToArray()));
        }

        static public void StartMultipleThread()
        {

            Task[] TaskArray = new Task[5];
            Double TotalCount = 1000.0;
            int Batches = (int)Math.Floor(TotalCount / TaskArray.Length);
            for (int i = 1; i <= TaskArray.Length; i++)
            {
                int ThreadID = i;
                int MaxCount = i * Batches;
                int Start = (MaxCount - Batches) + 1;
                TaskArray[i - 1] = Task.Factory.StartNew(() =>
                {
                    StartCount(Start, MaxCount, ThreadID);
                });
            }
            Task.WaitAll(TaskArray);
        }
        static public void StartCount(int Start, int MaxCount, int ThreadID)
        {
            for (int j = Start; j <= MaxCount; j++)
            {
                Console.WriteLine("{\"Worker\" : { \"Worker\" : " + ThreadID + ", \"Number\" : " + j);
            }
        }

        static public void SplitFile(string largeFileName, int chunkSize)
        {
            const int BUFFER_SIZE = 10 * 1024;
            byte[] Buffer = new byte[BUFFER_SIZE];
            //get system path and to be prepended to file name of smaller files
            var SystemPath = System.Environment.CurrentDirectory;
            //Read file
            using (Stream input = File.OpenRead(largeFileName))
            {
                //Append to small file name
                int FilePart = 0;
                //Output to file
                while (input.Position < input.Length)
                {
                    var completePath = Path.Combine(SystemPath, String.Format("Part{0}.txt", FilePart));
                    Console.WriteLine(completePath);
                    using (Stream output = File.Create(completePath))
                    {
                        int remaining = chunkSize, bytesRead;
                        Console.WriteLine(remaining);
                        while (remaining > 0 && (bytesRead = input.Read(Buffer, 0, Math.Min(remaining, BUFFER_SIZE))) > 0)
                        {
                            output.Write(Buffer, 0, bytesRead);
                            remaining -= bytesRead;
                        }
                    }
                    FilePart++;
                }
            }
        }

        static public void createMultipleMemeber()
        {
            var Members = new List<Member>();
            //Use 5 tasks for the job
            Task[] TaskArray = new Task[5];
            Double TotalCount = 1000.0;
            //Should have 200 members for each to create
            int Batches = (int)Math.Floor(TotalCount / TaskArray.Length);
            for (int i = 1; i <= TaskArray.Length; i++)
            {
                int ThreadID = i;
                int MaxCount = i * Batches;
                int Start = (MaxCount - Batches) + 1;
                TaskArray[i - 1] = Task.Factory.StartNew(() =>
                {
                    createSingleMember(Start, MaxCount, Members);
                });
            }
            Task.WaitAll(TaskArray);
            Console.WriteLine(Members.Count);
        }

        static public void createSingleMember(int Start, int MaxCount, List<Member> Members)
        {
            for(int j = Start; j <= MaxCount; j++)
            {
                Member member = new Member();
                member.Id = j;
                member.Birthday = RandomDay().ToShortDateString();
                member.Address = Faker.Address.StreetName() + " " + Faker.Address.City() + " " + Faker.Address.UsState() + " " + Faker.Address.ZipCode();
                member.Sex = j % 2 == 0 ? "Male" : "Female";

                lock (Members)
                {
                    Members.Add(member);
                }
            }
        }
    }
}


